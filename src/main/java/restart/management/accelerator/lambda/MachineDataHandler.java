/*
 * Copyright 2010-2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 */

/*
 * Demonstrates a simple publish to a topic using Greengrass Core Java SDK
 * This lambda function will retrieve underlying platform information and send
 * a hello world message along with the platform information to the topic
 * 'hello/world'. The function will sleep for five seconds, then repeat.
 * Since the function is long-lived it will run forever when deployed to a
 * Greengrass core.
 */

package restart.management.accelerator.lambda;

import java.util.Timer;

import com.amazonaws.services.lambda.runtime.Context;

public class MachineDataHandler {

	static {
		Timer timer = new Timer();
		// 1 hour configured time interval
		timer.scheduleAtFixedRate(new MachineDataHandlerTask(), 0, 1800000 * 20);
	}

	public String handleRequest(Object input, Context context) {
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>> MachineDataHandler.handleRequest : INVOKED");
		return "";
	}
}
