/*
 * Copyright 2010-2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 */

/*
 * Demonstrates a simple publish to a topic using Greengrass Core Java SDK
 * This lambda function will retrieve underlying platform information and send
 * a hello world message along with the platform information to the topic
 * 'hello/world'. The function will sleep for five seconds, then repeat.
 * Since the function is long-lived it will run forever when deployed to a
 * Greengrass core.
 */

package restart.management.accelerator.lambda;

import java.nio.ByteBuffer;
import java.util.List;
import java.util.TimerTask;

import com.amazonaws.greengrass.javasdk.IotDataClient;
import com.amazonaws.greengrass.javasdk.model.PublishRequest;

public class MachineDataHandlerTask extends TimerTask {

	public void run() {
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>> MachineDataHandlerTask.handleRequest : START");
		IotDataClient iotDataClient = new IotDataClient();

		System.out.println(
				">>>>>>>>>>>>>>>>>>>>>>>>>> MachineDataHandlerTask.handleRequest : Read Machine Data and Convert to JSON format");
		List<String> payload = FrameJSONPacketFromMachineData.getOutputStreamFromSocket();

		payload.parallelStream().forEach(e -> publishMessage(e, iotDataClient));
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>> MachineDataHandlerTask.handleRequest : COMPLETE");
	}

	private void publishMessage(String md, IotDataClient iotDataClient) {
		try {
			PublishRequest publishRequest = new PublishRequest().withTopic("emi/machine/data")
					.withPayload(ByteBuffer.wrap(md.getBytes()));
			iotDataClient.publish(publishRequest);
			System.out.println(
					">>>>>>>>>>>>>>>>>>>>>>>>>> MachineDataHandlerTask.publishMessage : Message Sent Successfully. MessageData: "
							+ md);

		} catch (Exception e) {
			System.out.println(
					">>>>>>>>>>>>>>>>>>>>>>>>>> Exception occurred while processing/publishing machine data to IoT Topic. Data: "
							+ md + ", ErrorMessage: " + e.getMessage());
			e.printStackTrace();
		}

	}
}
