package com.amazonaws.greengrass.pojo;

/**
 * 
 */
public class Component {
	
	// MD05
	private String componentName;
	// MD07
	private String dispensed;
	// MD07
	private String time;
	// MD06
	private ComponentStatistic componentStatistic;

	public String getComponentName() {
		return componentName;
	}

	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}

	public String getDispensed() {
		return dispensed;
	}

	public void setDispensed(String dispensed) {
		this.dispensed = dispensed;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public ComponentStatistic getComponentStatistic() {
		return componentStatistic;
	}

	public void setComponentStatistic(ComponentStatistic componentStatistic) {
		this.componentStatistic = componentStatistic;
	}

}