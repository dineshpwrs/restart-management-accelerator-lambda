package com.amazonaws.greengrass.pojo;

/**
 * 
 * @author RSTL032
 *
 */
public class ComponentStatistic //MD06
{
	private String placed;
	
	private String time;
	
	private String badDimension;
	
    private String badElectric;

    private String badPicked;

    private String badPlaced;

    private String badOther;

	public String getPlaced() {
		return placed;
	}

	public void setPlaced(String placed) {
		this.placed = placed;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getBadDimension() {
		return badDimension;
	}

	public void setBadDimension(String badDimension) {
		this.badDimension = badDimension;
	}

	public String getBadElectric() {
		return badElectric;
	}

	public void setBadElectric(String badElectric) {
		this.badElectric = badElectric;
	}

	public String getBadPicked() {
		return badPicked;
	}

	public void setBadPicked(String badPicked) {
		this.badPicked = badPicked;
	}

	public String getBadPlaced() {
		return badPlaced;
	}

	public void setBadPlaced(String badPlaced) {
		this.badPlaced = badPlaced;
	}

	public String getBadOther() {
		return badOther;
	}

	public void setBadOther(String badOther) {
		this.badOther = badOther;
	}

    

    
}
		