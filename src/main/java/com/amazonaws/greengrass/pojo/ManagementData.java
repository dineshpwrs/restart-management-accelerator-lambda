package com.amazonaws.greengrass.pojo;

import java.util.Date;
import java.util.List;

/**
 * 
 * @author RSTL032
 *
 */
public class ManagementData {

	private String trpTime;
	private Date date;
	private String layoutName; // MD00
	private String machineName; // MD01
	private String assembledLayouts; // MD02
	private String assembledBoards; // MD03
	private String assemblyTotalTime; // MD04_1
	private String assemblyDispenseTime; // MD04_2
	private String assemblyTime; // MD04_3
	private List<Component> component; // MD05&MD06&MD07

	public String getTrpTime() {
		return trpTime;
	}

	public void setTrpTime(String trpTime) {
		this.trpTime = trpTime;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getLayoutName() {
		return layoutName;
	}

	public void setLayoutName(String layoutName) {
		this.layoutName = layoutName;
	}

	public String getMachineName() {
		return machineName;
	}

	public void setMachineName(String machineName) {
		this.machineName = machineName;
	}

	public String getAssembledLayouts() {
		return assembledLayouts;
	}

	public void setAssembledLayouts(String assembledLayouts) {
		this.assembledLayouts = assembledLayouts;
	}

	public String getAssembledBoards() {
		return assembledBoards;
	}

	public void setAssembledBoards(String assembledBoards) {
		this.assembledBoards = assembledBoards;
	}

	public String getAssemblyTotalTime() {
		return assemblyTotalTime;
	}

	public void setAssemblyTotalTime(String assemblyTotalTime) {
		this.assemblyTotalTime = assemblyTotalTime;
	}

	public String getAssemblyDispenseTime() {
		return assemblyDispenseTime;
	}

	public void setAssemblyDispenseTime(String assemblyDispenseTime) {
		this.assemblyDispenseTime = assemblyDispenseTime;
	}

	public String getAssemblyTime() {
		return assemblyTime;
	}

	public void setAssemblyTime(String assemblyTime) {
		this.assemblyTime = assemblyTime;
	}

	public List<Component> getComponent() {
		return component;
	}

	public void setComponent(List<Component> component) {
		this.component = component;
	}

}